![GitHub Logo](/PNG/Logresse-Logo-With-Text.png)

***

This is the repo for the Logresse Long, the 200x300mm version of the Logresse. It's a work in progress and it has never been build. Be careful if you plan to use these blue prints. I suggest you wait till a version >= 1.0 is tagged.

You can figure out what it looks like with the OpenSCAD 3D preview, file LogresseLong.scad located in the SCAD/ directory.

***

Logresse is a 3D Printer made out of laser cut steel sheet, derivated from Irobri's P3-Steel (http://www.reprap.org/wiki/P3Steel)

License CC BY-NC 3.0 (see http://creativecommons.org/licenses/by-nc/3.0/)

***

**Information**

* Français : https://www.logre.eu/wiki/Logresse
* English  : https://www.logre.eu/wiki/Logresse/en
* Espanol  : https://www.logre.eu/wiki/Logresse/es
* Italiano : https://www.logre.eu/wiki/Logresse/it
* Deutsch  : https://www.logre.eu/wiki/Logresse/de
