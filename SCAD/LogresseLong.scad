// Logresse Long OpenSCAD 3D preview
// (c) Marc BERLIOUX, 2 décembre 2016

include <Bearings.scad>
include <Nema17.scad>
include <Pulleys.scad>
include <Couplers.scad>
include <Nuts.scad>
include <HeatedBeds.scad>
include <PowerSupply.scad>

/////// User Settings /////////////////////////////////////////////

// Axis Positions
XPosition=100;
YPosition=150;
ZPosition=50;

// Colors
FrameColor="Chartreuse";
XEndsColor="LightSalmon";
YCarriageColor="HotPink";
XCarriageColor="CornFlowerBlue";
RodsColor="PaleTurquoise";

// Show/Hide Assemblies or Elements ( Comment to hide )
showChassis=true;
showRods=true;
showMotors=true;
showPulleys=true;
showIdlers=true;
showBelts=true;
showBearings=true;
showThreadedRods=true;
showCouplers=true;
showXAssy=true;
showYCarriage=true;
showHeatedBed=true;
showPowerSupply=true;
//showXCarriageV1=true;
//showXCarriageV2=true;
showXCarriageV3=true;

// Rendering
$fs=0.5;
$fa=2.5;

/////////////////////////////////////////////////////////////////////

LogresseLong();

module LogresseLong (){
 if (showChassis) Chassis();

 if (showRods) {
  ZRods();
  YRods();
 }

 if(showMotors){
  YMotor();
  ZMotors();
 }

 if (showPulleys) YPulley();

 if (showIdlers) {translate([0,-FrontEndYShift+YIdlerYShift,YSupportsZShift]) rotate([0,90,0])YIdler();}

 if (showThreadedRods) ZThreadedRods();

 if (showCouplers) ZCouplers();

 if (showYCarriage) {
  translate([0,YAxisYPosition,YRodsZShift]){
   YCarriage();
   if (showBearings) YBearings();
  }
  if (showBelts) rotate([90,0,-90]) YBelt();
 }

 if (showXAssy) {
  translate([0,0,XAssyZPosition]) {
   translate([0,-XRodsYShift,0]) {
    translate([-ZRodsXShift,0,0]) XEndMotor();
    translate([ZRodsXShift,0,0]) XEndIdler();
    if (showRods) XRods();
    if (showMotors) XMotor();
    if (showPulleys) XPulley();
    if (showBelts) XBelt();
   }
   if (showBearings) ZBearings();
  }
 }

 if (showXCarriageV1) {
  translate([XCarriageXPosition,-XRodsYShift,XAssyZPosition]){
   XCarriageV1();
   if (showBearings) XCarriageV1Bearings();
  }
 }

 if (showXCarriageV2) {
  translate([XCarriageXPosition,-XRodsYShift,XAssyZPosition]){
   XCarriageV2();
   if (showBearings) XCarriageV2Bearings();
  }
 }

 if (showXCarriageV3) {
  translate([XCarriageXPosition,-XRodsYShift,XAssyZPosition]){
   XCarriageV3();
   if (showBearings) XCarriageV3Bearings();
  }
 }
 
 if (showPowerSupply)
  translate([SidesXShift+SheetThickness/2,PowerSupplyYShift,PowerSupplyZShift]) 
  rotate([0,90,0])PowerSupply();
}

SheetThickness=3;
RodsDiameter=8;
BeltsThickness=1;
LM8UUToPlate=6;
PowerSupplyYShift=68;
PowerSupplyZShift=193; // range 149-238 ; middle position = 193

SidesXShift=120.175;
FrontEndYShift=284;
BackEndYShift=133;
FullTopZShift=366.9;
RodHolderShift=12;
FrameCornerZShift=25;
FrameCornerXShift=58.525;
EndCornerXShift=90.175;
EndCornerZShift=25;
FrameSideCornerXShift=165.3;
FrameSideCornerZShift=40;
HeatedBedThickness=1.6;
HeatedBedZShift=20;

ZRodsXShift=182.3;
ZRodsYShift=37.3;
ZMotorSupportZShift=51.645;
ZMotorSupportXShift=ZRodsXShift-17;
ZMotorCornerZShift=25;
ZMotorCornerXShift=24.52; // according to ZMotorSupportXShift..
ZMotorAxisYShift=37.3;
ZCoupler2NemaZShift=25;
ZRodsLength=FullTopZShift-ZMotorSupportZShift+SheetThickness*1.5;
ZRodsZShift=ZRodsLength/2+ZMotorSupportZShift-SheetThickness/2;

YMotorAxisYShift=31.5; // according to Ends
YMotorAxisZShift=5; // according to Y Supports screw
YSupportsXShift=8.3; // according to Ends center
YSupportsZShift=25.3876;
YRodsXShift=85.0025;
YRodsYShift=(FrontEndYShift-BackEndYShift+SheetThickness)/2;
YRodsZShift=49.3893;
YBearingsYShift=35; // according to Bed center
YRodsLength=FrontEndYShift+BackEndYShift+SheetThickness;
YBeltSupportSidesXShift=5.5;
YBeltSupportTopZShift=18.1; // according to Bed
YBeltHolderYShift=10;
YIdlerYShift=19.67;

XEndFork2Plate=7.65685; // according to X Ends plates
XRodsSpacing=45;
XRodsLength=(ZRodsXShift+22)*2;
XRodsYShift=ZRodsYShift-LM8UUToPlate-SheetThickness-XEndFork2Plate;
XEndForkXShift=17.5; // according to bearings
XEndsBearingsZShift=17.5; // according to X Axis
XMotorAxisXShift=45.4203; // according to Z
XIdlerXShift=27;

XCarriageV1ExtruderZShift=-2.95;
XCarriageV1BearingsXShift=21;
XCarriageV1CornerZShift=21;
XCarriageV1CornerXShift=32;
XCarriageV1BeltXShift=9;

XCarriageV2ExtruderZShift=10.3;
XCarriageV2BearingsXShift=21.025;
XCarriageV2CornerZShift=15;
XCarriageV2CornerXShift=34;
XCarriageV2BeltXShift=7;
XCarriageV2HeadYShift=48;

XCarriageV3ExtruderZShift=10.3;
XCarriageV3BearingsXShift=17;
XCarriageV3CornerZShift=15;
XCarriageV3CornerXShift=34;
XCarriageV3BeltXShift=16;
XCarriageV3HeadYShift=48;

XAssyZPosition=FullTopZShift-32.5-200+ZPosition;
YAxisYPosition=-FrontEndYShift+YBearingsYShift+12+310-YPosition;
XCarriageXPosition=XPosition-100;

echo("<b>Stubs X : ",XRodsLength,"mm</b>");
echo("<b>Stubs Y : ",YRodsLength,"mm</b>");
echo("<b>Stubs Z : ",ZRodsLength,"mm</b>");

module Chassis(){
 color(FrameColor) {
 // Frame
  rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/frame.dxf",layer="PRINCIPAL");
  // Left Side
  translate([SidesXShift-SheetThickness/2,0,0]) rotate([90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/left-side_Long.dxf",layer="PRINCIPAL");
  // Right Side
  translate([-SidesXShift+SheetThickness/2,0,0]) rotate([90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/right-side_Long.dxf",layer="PRINCIPAL");
  // Front End
  translate([0,-FrontEndYShift,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/end.dxf",layer="PRINCIPAL");
  // Back End
  translate([0,BackEndYShift,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/end.dxf",layer="PRINCIPAL");
  // Full Top
  translate([0,0,FullTopZShift])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/full-top.dxf",layer="PRINCIPAL");
  // Frame Corners
  translate([FrameCornerXShift,0,FrameCornerZShift-SheetThickness/2])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/frame-corner.dxf",layer="PRINCIPAL");
  translate([-FrameCornerXShift,0,FrameCornerZShift+SheetThickness/2]) rotate([0,180,0]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/frame-corner.dxf",layer="PRINCIPAL");
  // Ends Corners
  translate([-EndCornerXShift,-FrontEndYShift,EndCornerZShift-SheetThickness/2])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/end-corner.dxf",layer="PRINCIPAL");
  translate([EndCornerXShift,-FrontEndYShift,EndCornerZShift+SheetThickness/2]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/end-corner.dxf",layer="PRINCIPAL");
  translate([-EndCornerXShift,BackEndYShift-SheetThickness,EndCornerZShift+SheetThickness/2]) rotate([180,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/end-corner.dxf",layer="PRINCIPAL");
  translate([EndCornerXShift,BackEndYShift-SheetThickness,EndCornerZShift-SheetThickness/2]) rotate([180,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/end-corner.dxf",layer="PRINCIPAL");
  // Frame Side Corners
  translate([FrameSideCornerXShift,0,FrameSideCornerZShift-SheetThickness/2]) rotate([0,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/frame-side-corner.dxf",layer="PRINCIPAL");
  translate([-FrameSideCornerXShift,0,FrameSideCornerZShift+SheetThickness/2]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/frame-side-corner.dxf",layer="PRINCIPAL");
  // Z motors supports
  translate([ZMotorSupportXShift,0,ZMotorSupportZShift-SheetThickness/2]) rotate([0,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/z-motor-support.dxf",layer="PRINCIPAL");
  translate([-ZMotorSupportXShift,0,ZMotorSupportZShift+SheetThickness/2]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/z-motor-support.dxf",layer="PRINCIPAL");
  // Z motors corners
  translate([ZMotorSupportXShift-ZMotorCornerXShift-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/z-motor-corner.dxf",layer="PRINCIPAL");
  translate([ZMotorSupportXShift+ZMotorCornerXShift-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/z-motor-corner.dxf",layer="PRINCIPAL");
  translate([-ZMotorSupportXShift+ZMotorCornerXShift-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/z-motor-corner.dxf",layer="PRINCIPAL");
  translate([-ZMotorSupportXShift-ZMotorCornerXShift-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/z-motor-corner.dxf",layer="PRINCIPAL");
  // Y Motor Supports
  translate([-YSupportsXShift-SheetThickness/2,BackEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-motor-support.dxf",layer="PRINCIPAL");
  translate([YSupportsXShift-SheetThickness/2,BackEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-motor-support.dxf",layer="PRINCIPAL");
  // Y Idler Supports
  translate([-YSupportsXShift-SheetThickness/2,-FrontEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-idler-support.dxf",layer="PRINCIPAL");
  translate([YSupportsXShift-SheetThickness/2,-FrontEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-idler-support.dxf",layer="PRINCIPAL");
  // Y Idler Strainers
  translate([-YSupportsXShift+SheetThickness/2,-FrontEndYShift+YIdlerYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-idler-strainer.dxf",layer="PRINCIPAL");
  translate([YSupportsXShift-SheetThickness*3/2,-FrontEndYShift+YIdlerYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-idler-strainer.dxf",layer="PRINCIPAL");
  // Y Rod Holders
  translate([YRodsXShift,-FrontEndYShift-SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/rod-holder.dxf",layer="PRINCIPAL");
  translate([-YRodsXShift,-FrontEndYShift-SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/rod-holder.dxf",layer="PRINCIPAL");
  translate([YRodsXShift,BackEndYShift+SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/rod-holder.dxf",layer="PRINCIPAL");
  translate([-YRodsXShift,BackEndYShift+SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/rod-holder.dxf",layer="PRINCIPAL");
  // Z Rod Holders
  translate([ZRodsXShift,-ZRodsYShift+RodHolderShift,FullTopZShift+SheetThickness]) rotate([0,0,180])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/rod-holder.dxf",layer="PRINCIPAL");
  translate([-ZRodsXShift,-ZRodsYShift+RodHolderShift,FullTopZShift+SheetThickness]) rotate([0,0,180])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/rod-holder.dxf",layer="PRINCIPAL");
 }
}

module ZRods(){
 color(RodsColor){
  translate([ZRodsXShift,-ZRodsYShift,ZRodsZShift])
  cylinder(r=RodsDiameter/2,h=ZRodsLength,center=true);
  translate([-ZRodsXShift,-ZRodsYShift,ZRodsZShift])
  cylinder(r=RodsDiameter/2,h=ZRodsLength,center=true);
 }
}

module YRods(){
 color(RodsColor){
  translate([YRodsXShift,-YRodsYShift,YRodsZShift])rotate([90,0,0])
  cylinder(r=RodsDiameter/2,h=YRodsLength,center=true);
  translate([-YRodsXShift,-YRodsYShift,YRodsZShift])rotate([90,0,0])
  cylinder(r=RodsDiameter/2,h=YRodsLength,center=true);
 }
}

module XRods(){
 color(RodsColor){
   translate([0,0,XRodsSpacing/2])rotate([0,90,0])
   cylinder(r=RodsDiameter/2,h=XRodsLength,center=true);
   translate([0,0,-XRodsSpacing/2])rotate([0,90,0])
   cylinder(r=RodsDiameter/2,h=XRodsLength,center=true);  
 }
}

module XEndMotor(){
 color(XEndsColor) {
  // X End Motor Bearings
  translate([0,-XEndFork2Plate-SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-motor-bearings.dxf",layer="PRINCIPAL");
  // X End Motor Back
  translate([0,XEndFork2Plate+SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-motor-back.dxf",layer="PRINCIPAL");
  // X Ends Forks
  translate([XEndForkXShift,0,0]) rotate([90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  translate([XEndForkXShift,0,0]) rotate([90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  translate([-XEndForkXShift,0,0]) rotate([-90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  translate([-XEndForkXShift,0,0]) rotate([-90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  // X Nut Clamp
  translate([ZRodsXShift-ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,SheetThickness])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-5mm-bread.dxf",layer="PRINCIPAL");
  translate([ZRodsXShift-ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,0]) rotate([0,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-5mm-ham.dxf",layer="PRINCIPAL");
  translate([ZRodsXShift-ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,-SheetThickness])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-5mm-bread.dxf",layer="PRINCIPAL");
 }
 // Nut
 color("Silver"){translate([ZRodsXShift-ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,0])Nut(8,3.4,2.7);}
}

module XEndIdler(){
 color(XEndsColor) {
  // X End Idler Bearings
  translate([0,-XEndFork2Plate-SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-idler-bearings.dxf",layer="PRINCIPAL");
  // X End Idler Back
  translate([0,XEndFork2Plate+SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-idler-back.dxf",layer="PRINCIPAL");
  // X Ends Forks
  translate([0+XEndForkXShift,0,0]) rotate([90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  translate([0+XEndForkXShift,0,0]) rotate([90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  translate([0-XEndForkXShift,0,0]) rotate([-90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  translate([0-XEndForkXShift,0,0]) rotate([-90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/x-end-fork.dxf",layer="PRINCIPAL");
  // X Nut Clamp
  translate([-ZRodsXShift+ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,0+SheetThickness]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-5mm-bread.dxf",layer="PRINCIPAL");
  translate([-ZRodsXShift+ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,0]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-5mm-ham.dxf",layer="PRINCIPAL");
  translate([-ZRodsXShift+ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,0-SheetThickness]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/x-end-5mm-bread.dxf",layer="PRINCIPAL");
 }
 color("Silver"){translate([-ZRodsXShift+ZMotorSupportXShift,-LM8UUToPlate-SheetThickness-XEndFork2Plate,0])Nut(8,3.4,2.7);}
 if (showIdlers){translate([XIdlerXShift,0,0])rotate([90,0,0])XIdler();}
}

module XIdler(){
 // 2x MF115-ZZ
 translate([0,0,2]) FlangedBallBearing(5,11,4,13,1);
 translate([0,0,-2])rotate([180,0,0]) FlangedBallBearing(5,11,4,13,1);
}

module XBelt(){
 rotate([90,0,0])
 color("Black")linear_extrude(height=6, center=true, convexity=5) import("XBelt.dxf",layer="XBelt");
}

module YBelt(){
 color("Black")linear_extrude(height=6, center=true, convexity=5) import("YBelt_Long.dxf",layer="YBelt");
}
 
module YIdler(){
 // 1x 608ZZ
 BallBearing(8,22,7);
}

module YCarriage(){
 color(YCarriageColor) {
  // Bed
  translate([0,0,LM8UUToPlate])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/bed_Long.dxf",layer="PRINCIPAL");
  // Y Belt Holder
  translate([YBeltSupportSidesXShift,-YBeltHolderYShift,LM8UUToPlate]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/y-belt-holder-side-left.dxf",layer="PRINCIPAL");
  translate([-YBeltSupportSidesXShift,-YBeltHolderYShift,LM8UUToPlate]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/y-belt-holder-side-right.dxf",layer="PRINCIPAL");
  translate([0,-YBeltHolderYShift,LM8UUToPlate-YBeltSupportTopZShift])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-belt-holder-slotted.dxf",layer="PRINCIPAL");
  translate([0,-YBeltHolderYShift,LM8UUToPlate-YBeltSupportTopZShift-SheetThickness*2-0.2])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/y-belt-holder-threaded.dxf",layer="PRINCIPAL");
 }
 // Heated Bed
 if (showHeatedBed) { translate([0,0,LM8UUToPlate+HeatedBedZShift+HeatedBedThickness/2]) rotate([0,0,90]) HeatedBed314x214(); }
}

module XCarriageV1Bearings(){
 translate([0,0,-XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
 translate([XCarriageV1BearingsXShift,0,XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
 translate([-XCarriageV1BearingsXShift,0,XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
}

module XCarriageV2Bearings(){
 translate([0,0,-XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
 translate([XCarriageV2BearingsXShift,0,XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
 translate([-XCarriageV2BearingsXShift,0,XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
}

module XCarriageV3Bearings(){
 translate([0,0,XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
 translate([XCarriageV3BearingsXShift,0,-XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
 translate([-XCarriageV3BearingsXShift,0,-XRodsSpacing/2]) rotate([0,90,0])LMxxUU();
}

module ZBearings(){
 translate([-ZRodsXShift,-ZRodsYShift,XEndsBearingsZShift]) LMxxUU();
 translate([-ZRodsXShift,-ZRodsYShift,-XEndsBearingsZShift]) LMxxUU();
 translate([ZRodsXShift,-ZRodsYShift,XEndsBearingsZShift]) LMxxUU();
 translate([ZRodsXShift,-ZRodsYShift,-XEndsBearingsZShift]) LMxxUU();
}

module YBearings(){
 translate([YRodsXShift,YBearingsYShift,0])rotate([90,0,0])LMxxUU();
 translate([YRodsXShift,-YBearingsYShift,0])rotate([90,0,0])LMxxUU();
 translate([-YRodsXShift,YBearingsYShift,0])rotate([90,0,0])LMxxUU();
 translate([-YRodsXShift,-YBearingsYShift,0])rotate([90,0,0])LMxxUU();
}

module XMotor(){
 translate([-ZRodsXShift-XMotorAxisXShift,XEndFork2Plate+SheetThickness,0]) rotate([-90,45,0]) Nema17();
}

module YMotor(){
 translate([-YSupportsXShift-SheetThickness/2,BackEndYShift-YMotorAxisYShift,YSupportsZShift+YMotorAxisZShift]) rotate([0,-90,0]) Nema17();
}

module ZMotors(){
 translate([-ZMotorSupportXShift,-ZMotorAxisYShift,ZMotorSupportZShift-SheetThickness/2]) rotate([180,0,0]) Nema17();
 translate([ZMotorSupportXShift,-ZMotorAxisYShift,ZMotorSupportZShift-SheetThickness/2]) rotate([180,0,0]) Nema17();
}

module XPulley(){
 translate([-ZRodsXShift-XMotorAxisXShift,0,0]) rotate([90,0,0]) 20TeethGT2Pulley();
}
 
module YPulley(){
 translate([0,BackEndYShift-YMotorAxisYShift,YSupportsZShift+YMotorAxisZShift]) rotate([0,90,0]) 20TeethGT2Pulley();
}
 
module XCarriageV1(){
 color(XCarriageColor) {
  // X Carriage Bearings
  translate([0,-LM8UUToPlate,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V1/x-carriage-bearings.dxf",layer="PRINCIPAL");
  // X Carriage Extruder
  translate([0,0,XCarriageV1ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V1/x-carriage-extruder.dxf",layer="PRINCIPAL");
  // X Carriage Corners
  translate([XCarriageV1CornerXShift,0,-XCarriageV1CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/X-Carriage_V1/x-carriage-corner.dxf",layer="PRINCIPAL");
  translate([-XCarriageV1CornerXShift,0,-XCarriageV1CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/X-Carriage_V1/x-carriage-corner.dxf",layer="PRINCIPAL");
  // X Carriage Belts
  translate([XCarriageV1BeltXShift,0,XCarriageV1ExtruderZShift-SheetThickness*2-BeltsThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V1/x-carriage-belt.dxf",layer="PRINCIPAL");
  translate([-XCarriageV1BeltXShift,0,XCarriageV1ExtruderZShift-SheetThickness*2-BeltsThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V1/x-carriage-belt.dxf",layer="PRINCIPAL");
 }
}

module XCarriageV2(){
 color(XCarriageColor) {
  // X Carriage Bearings V2
  translate([0,-LM8UUToPlate,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-bearings_v2.dxf",layer="PRINCIPAL");
  // X Carriage Extruder V2
  translate([0,0,XCarriageV2ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-extruder_v2.dxf",layer="PRINCIPAL");
  // X Carriage Head V2
  translate([0,-XCarriageV2HeadYShift,XCarriageV2ExtruderZShift-SheetThickness*2]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-head_v2.dxf",layer="PRINCIPAL");
  // X Carriage Corner Fanduct V2
  translate([XCarriageV2CornerXShift,0,-XCarriageV2CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-corner-fanduct_v2.dxf",layer="PRINCIPAL");
  // X Carriage Corner Servo V2
  translate([-XCarriageV2CornerXShift,0,-XCarriageV2CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-corner-servo_v2.dxf",layer="PRINCIPAL");
  // X Carriage Belt Support V2
  translate([0,0,XCarriageV2ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-belt-support_v2.dxf",layer="PRINCIPAL");
  // X Carriage Belts V2
  translate([XCarriageV2BeltXShift,0,XCarriageV2ExtruderZShift-SheetThickness*2-BeltsThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-belt_v2.dxf",layer="PRINCIPAL");
  translate([-XCarriageV2BeltXShift,0,XCarriageV2ExtruderZShift-SheetThickness-BeltsThickness]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V2/x-carriage-belt_v2.dxf",layer="PRINCIPAL");
 }
}

module XCarriageV3(){
 color(XCarriageColor) {
  // X Carriage Bearings V3
  translate([0,-LM8UUToPlate,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-bearings_v3.dxf",layer="PRINCIPAL");
  // X Carriage Extruder V3
  translate([0,0,XCarriageV3ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-extruder_v3.dxf",layer="PRINCIPAL");
  // X Carriage Head V3
  translate([0,-XCarriageV3HeadYShift,XCarriageV3ExtruderZShift-SheetThickness*2]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-head_v3.dxf",layer="PRINCIPAL");
  // X Carriage Corner Fanduct V3
  translate([XCarriageV3CornerXShift,0,-XCarriageV3CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-corner-fanduct_v3.dxf",layer="PRINCIPAL");
  // X Carriage Corner Servo V3
  translate([-XCarriageV3CornerXShift,0,-XCarriageV3CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-corner-servo_v3.dxf",layer="PRINCIPAL");
  // X Carriage Belt Support V3
  translate([0,0,XCarriageV3ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-belt-support_v3.dxf",layer="PRINCIPAL");
  // X Carriage Belts V3
  translate([XCarriageV3BeltXShift,0,XCarriageV3ExtruderZShift-SheetThickness*2-BeltsThickness*2]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-belt_v3.dxf",layer="PRINCIPAL");
  translate([-XCarriageV3BeltXShift,0,XCarriageV3ExtruderZShift-SheetThickness-BeltsThickness*2]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/200x300/X-Carriage_V3/x-carriage-belt_v3.dxf",layer="PRINCIPAL");
 }
}

module ZCouplers(){
 translate([ZMotorSupportXShift,-ZMotorAxisYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])coupler();
 translate([-ZMotorSupportXShift,-ZMotorAxisYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])coupler();
}

module ZThreadedRods(){
 color("Silver") {
  translate([ZMotorSupportXShift,-ZMotorAxisYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])
  cylinder(r=2.5,h=300,center=false,$fn=64);
  translate([-ZMotorSupportXShift,-ZMotorAxisYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])
  cylinder(r=2.5,h=300,center=false,$fn=64);
 }
}

