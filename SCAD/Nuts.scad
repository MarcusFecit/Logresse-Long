// Nuts
// (c) Marc BERLIOUX, 17 novembre 2016

// Nut();

module Nut(dWrench=13,dThread=8,nHeight=6.5){
 color("Gainsboro") {
  difference(){
   cylinder(r=dWrench/2*0.866,h=nHeight,center=true,$fn=6);
   cylinder(r=dThread/2,h=nHeight+1,center=true);
  }
 } 
}

module Washer(dIn=8,dOut=26,wThickness=1.2){
 color("Gainsboro") {
  difference(){
   cylinder(r=dOut/2,h=wThickness,center=true);
   cylinder(r=dIn/2,h=wThickness+1,center=true);
  }
 }
}
