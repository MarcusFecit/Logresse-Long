// HeatedBeds
// (c) Marc BERLIOUX, 17 novembre 2016

//HeatedBed214x214();
//HeatedBed314x214();

module HeatedBed214x214(){
 color("Red") {
  difference(){
   linear_extrude(height=1.6, center=true, convexity=5) import("HeatedBeds.dxf",layer="214x214");
   linear_extrude(height=2, center=true, convexity=5) import("HeatedBeds.dxf",layer="214x214Holes");
  }
 }
  translate([0,0,0.85])color("Turquoise") linear_extrude(height=0.1, center=true, convexity=5) import("HeatedBeds.dxf",layer="214x214Silk1");
  translate([0,0,0.85])color("Turquoise") linear_extrude(height=0.1, center=true, convexity=5) import("HeatedBeds.dxf",layer="214x214Silk2");
}

module HeatedBed314x214(){
 color("Red") {
  difference(){
   linear_extrude(height=1.6, center=true, convexity=5) import("HeatedBeds.dxf",layer="314x214");
   linear_extrude(height=2, center=true, convexity=5) import("HeatedBeds.dxf",layer="314x214Holes");
  }
 }
  translate([0,0,0.85])color("Turquoise") linear_extrude(height=0.1, center=true, convexity=5) import("HeatedBeds.dxf",layer="314x214Silk1");
  translate([0,0,0.85])color("Turquoise") linear_extrude(height=0.1, center=true, convexity=5) import("HeatedBeds.dxf",layer="314x214Silk2");
}
