// Pulleys
// (c) Marc BERLIOUX, 17 novembre 2016

//20TeethGT2Pulley();

module 20TeethGT2Pulley(shaft=5){
 color("Azure") {
  difference(){
   union(){
    linear_extrude(height=8, center=true, convexity=5) import("Pulleys.dxf",layer="20Teeth", $fn=64);
    translate([0,0,8])linear_extrude(height=8, center=true, convexity=5) import("Pulleys.dxf",layer="Outside", $fn=64);
    translate([0,0,-4.5])linear_extrude(height=1, center=true, convexity=5) import("Pulleys.dxf",layer="Outside", $fn=64);
   }
   cylinder(r=shaft/2,h=25, center=true, $fn=64);
  }
 }
}